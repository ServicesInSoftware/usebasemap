using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using Esri.ArcGISRuntime.Data;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Location;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.Security;
using Esri.ArcGISRuntime.Symbology;
using Esri.ArcGISRuntime.Tasks;
using Esri.ArcGISRuntime.UI;
using Esri.ArcGISRuntime.Portal;

namespace UseBasemap.Shared
{
    /// <summary>
    /// Provides map data to an application
    /// </summary>
    public class MapViewModel : INotifyPropertyChanged
    {



        public MapViewModel()
        {

        }

        // String array to store basemap constructor types
        private string[] _basemapTypes = new string[]
        {
            "Topographic",
            "Topographic Vector",
            "Streets",
            "Streets Vector",
            "Imagery",
            "Oceans",
            "USGS National Map",
            "World Globe 1812",
            "BlueReview",
            "BlueReview Cred",
            "Goochland"

        };

        private Map _map = new Map(Basemap.CreateStreetsVector());

        /// <summary>
        /// Gets or sets the map
        /// </summary>
        public Map Map
        {
            get { return _map; }
            set { _map = value; OnPropertyChanged(); }
        }

        /// <summary>
        /// Raises the <see cref="MapViewModel.PropertyChanged" /> event
        /// </summary>
        /// <param name="propertyName">The name of the property that has changed</param>
        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var propertyChangedHandler = PropertyChanged;
            if (propertyChangedHandler != null)
                propertyChangedHandler(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string[] BasemapChoices
        {
            get { return _basemapTypes; }
        }

        public async void ChangeBasemap(string basemap)
        {
            // Apply the selected basemap to the map
            switch (basemap)
            {
                case "Topographic":
                    // Set the basemap to Topographic
                    Map = new Map(Basemap.CreateTopographic());
                    break;
                case "Topographic Vector":
                    // Set the basemap to Topographic (vector)
                    Map = new Map(Basemap.CreateTopographicVector());
                    break;
                case "Streets":
                    // Set the basemap to Streets
                    Map = new Map(Basemap.CreateStreets());
                    break;
                case "Streets Vector":
                    // Set the basemap to Streets (vector)
                    Map = new Map(Basemap.CreateStreetsVector());
                    break;
                case "Imagery":
                    // Set the basemap to Imagery
                    Map = new Map(Basemap.CreateImagery());
                    break;
                case "Oceans":
                    // Set the basemap to Oceans
                    Map = new Map(Basemap.CreateOceans());
                    break;
                case "USGS National Map":
                    // Set the basemap to USGS National Map by opening it from ArcGIS Online
                    var itemID = "809d37b42ca340a48def914df43e2c31";

                    // Connect to ArcGIS Online
                    ArcGISPortal agsOnline = await ArcGISPortal.CreateAsync();

                    // Get the USGS webmap item
                    PortalItem usgsMapItem = await PortalItem.CreateAsync(agsOnline, itemID);

                    // Create a new basemap using the item
                    Map = new Map(new Basemap(usgsMapItem));
                    break;
                case "World Globe 1812":
                    // Create a URI that points to a map service to use in the basemap
                    var uri = new Uri("https://tiles.arcgis.com/tiles/IEuSomXfi6iB7a25/arcgis/rest/services/World_Globe_1812/MapServer");

                    // Create an ArcGISTiledLayer from the URI
                    ArcGISTiledLayer baseLayer = new ArcGISTiledLayer(uri);

                    // Create a basemap from the layer and assign it to the map
                    Map = new Map(new Basemap(baseLayer));
                    break;
                case "BlueReview":
                    Map = await CreateFromChallenge("fe5750edc497448e99f4cad6c6514b0c");

                    break;
                case "BlueReview Cred":
                    //var mapId = "83e672a5b4844b87ab8f963611f3439a"; // Doesn't Work
                    //var mapId = "fe5750edc497448e99f4cad6c6514b0c"; // Works
                    //var mapId = "7edead16f27b4bedab5304f509696f2a";
                    var mapId = "07b67a376e06451e834071a8e8e79b67"; // New version
                    Map = await CreateFromCredential(mapId);
                    break;
                case "Goochland":
                    Uri goochlandUri = new Uri(@"http://gis.co.goochland.va.us/arcgis01/rest/services/Blue_Review_Service/GC_Util_BlueReview/MapServer");
                    // Create new image layer from the url
                    ArcGISMapImageLayer imageLayer = new ArcGISMapImageLayer(goochlandUri);


                    Map = new Map(Basemap.CreateTopographic());
                    Map.OperationalLayers.Add(imageLayer);                   
                    break;
                   

            }
        }
        /// <summary>
        /// Create  a Map from a mapId assuming that the app has access to the user's username and password
        /// </summary>
        /// <param name="mapId"></param>
        /// <returns></returns>
        public async Task<Map> CreateFromCredential(string mapId)
        {
            // Connect to ArcGIS Online
            ArcGISPortal agsOnline1 = await ArcGISPortal.CreateAsync();
            AuthenticationManager.Current.ChallengeHandler = new ChallengeHandler(Challenge);
            string url = string.Format(@"http://www.arcgis.com/home/item.html?id={0}", mapId);

            // Get the USGS webmap item
            PortalItem bluereviewItem = await PortalItem.CreateAsync(new Uri(url));

            // Create a new basemap using the item
            return new Map(bluereviewItem);

        }


        /// <summary>
        /// Create a Map from an arcgis.com mapId relying on ESRI runtime to challenge for username and password
        /// </summary>
        /// <param name="mapId"></param>
        /// <returns></returns>
        public  async Task<Map> CreateFromChallenge(string mapId)
        {
            AuthenticationManager authManager = AuthenticationManager.Current;

            string ServerUrl = @"http://www.arcgis.com/home/";

            string url = string.Format(@"http://www.arcgis.com/home/item.html?id={0}", mapId);

            Uri mapUri = new Uri(url);
            try
            {

                var serverInfo = new ServerInfo
                {
                    ServerUri = mapUri, // new Uri(ServerUrl),
                    TokenAuthenticationType = TokenAuthenticationType.OAuthClientCredentials,
                    TokenServiceUri = new Uri(@"https://www.arcgis.com/sharing/rest/oauth2"),
                };

            }
            catch (Exception e)
            {
                return null;
            }

            return new Map(mapUri);
        }

        private async Task<Credential> Challenge(CredentialRequestInfo arg)
        {
            AuthenticationManager authManager = AuthenticationManager.Current;
            string ServerUrl = @"http://www.arcgis.com/home/";  // your portal url
            Credential cred = null;

            try
            {
                CredentialRequestInfo cri = new CredentialRequestInfo
                {
                    // token authentication
                    AuthenticationType = AuthenticationType.Token,
                    // define the service URI
                    ServiceUri = new Uri(ServerUrl),
                    // OAuth (implicit flow) token type
                    GenerateTokenOptions = new GenerateTokenOptions
                    {
                        TokenAuthenticationType = TokenAuthenticationType.OAuthImplicit
                    }
                };


                var tokenServiceUri = new Uri("https://www.arcgis.com/sharing/rest/oauth2/token");  // url for generating token
                cred = await authManager.GenerateCredentialAsync(tokenServiceUri, "br_dev", "8EJrQ9zN");

                authManager.AddCredential(cred);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

            return cred;
        }
    }
}