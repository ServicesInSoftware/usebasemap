﻿using Xamarin.Forms;

namespace UseBasemap
{
	public partial class MapPage : ContentPage
    {
        public MapPage()
        {
            InitializeComponent();
            // Get the view model (defined as a resource in the XAML)

            _mapViewModel = this.Resources["MapViewModel"] as Shared.MapViewModel;

            // Define a click handler for the Basemaps button (show the basemap choice list)
            BasemapsButton.Clicked += (s, e) => BasemapListBox.IsVisible = true;

            // Define a selection handler on the basemap list
            BasemapListBox.ItemTapped += OnBasemapsClicked;

            Device.OnPlatform(
    Android: () =>
    {
        // Black background on Android (transparent by default)
        BasemapListBox.BackgroundColor = Color.Black;
    },
    WinPhone: () =>
    {
        // Semi-transparent background on Windows with a small margin around the control
        BasemapListBox.BackgroundColor = Color.FromRgba(255, 255, 255, 0.3);
        BasemapListBox.Margin = new Thickness(50);
    });
        }

        private Shared.MapViewModel _mapViewModel;

        // Map initialization logic is contained in MapViewModel.cs

        private void OnBasemapsClicked(object sender, ItemTappedEventArgs e)
        {
            // Get the text (basemap name) selected in the list box
            var basemapName = e.Item.ToString();

            // Pass the basemap name to the view model method to change the basemap
            _mapViewModel.ChangeBasemap(basemapName);
            // Hide the basemap list
            BasemapListBox.IsVisible = false;
        }
    }
}
